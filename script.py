import uuid
from datetime import datetime
import requests
from database import database

response = requests.get("https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes-disponibilites&rows=100&timezone=Europe%2FParis")
jsonResult = response.json()
allParking = jsonResult.get("records")
print(len(allParking))

compt=0

for AllfieldsParking in allParking :
    compt= compt+1
    print(compt)
    fieldParking = AllfieldsParking.get("fields")
    horaire = fieldParking.get("grp_horodatage")
    nbPlaceDispo = fieldParking.get("grp_disponible")
    idParking = fieldParking.get("idobj")
    date = datetime.strptime(str(horaire), "%Y-%m-%dT%H:%M:%S+01:00")
    date = date.strftime('%Y-%m-%d ') + str(int(date.strftime('%H'))) + ":00:00" if int(
        date.strftime('%H')) > 9 else + date.strftime('%m-%d ') + "0" + str(
        int(date.strftime('%H'))) + ":00:00"

    responseParkingNbMax = requests.get("https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes&q=&facet=idobj&refine.idobj=" + idParking)
    jsonResultParkingNbMax = responseParkingNbMax.json()
    print(jsonResultParkingNbMax.get("records"))

    if len(jsonResultParkingNbMax.get("records")) != 0:
        allResult = jsonResultParkingNbMax.get("records")
        fieldsResult = allResult[0].get("fields")
        if(fieldsResult.get("capacite_voiture") and fieldsResult.get("capacite_moto") and fieldsResult.get("capacite_vehicule_electrique") ) :
            somme = int(fieldsResult.get("capacite_voiture")) + int(fieldsResult.get("capacite_moto")) + int(fieldsResult.get("capacite_vehicule_electrique"))
        elif (fieldsResult.get("capacite_moto") is None and fieldsResult.get("capacite_vehicule_electrique")):
            somme = int(fieldsResult.get("capacite_voiture")) + int(fieldsResult.get("capacite_vehicule_electrique"))

        elif(fieldsResult.get("capacite_vehicule_electrique") is None and fieldsResult.get("capacite_moto")):
             somme = int(fieldsResult.get("capacite_voiture")) + int(fieldsResult.get("capacite_moto"))
        else:
            somme = int(fieldsResult.get("capacite_voiture"))

        nbPlacePrise = somme - nbPlaceDispo
        if nbPlacePrise < 0:
            nbPlacePrise = 0
    else:
        somme = 0
        nbPlacePrise = 0
    db = database()
    conn = db.connection
    cur = conn.cursor()
    identifiant = uuid.uuid4()
    identifiant = str(identifiant)
    nbPlacePrise = str(nbPlacePrise)
    somme = str(somme)
    # requestBuilder = "INSERT INTO remplissage (id,date,nbplaceoccupe,idparking,nbplacemax) values ('"+ identifiant +"','"+ date +"','" + idParking +"'," + nbPlacePrise + ")"
    cur.execute("INSERT INTO remplissage (id,date,nbplaceoccupe,idparking,nbplacemax) values ('"+ identifiant +"','"+ date +"', " + nbPlacePrise + ",'" + idParking + "'," + somme + ")")
    conn.commit()
# print(jsonResult.get("records"))